<?php /* Smarty version Smarty-3.1.13, created on 2016-04-27 10:39:38
         compiled from "/home/darkvictor/public_html/presta_shop/prestashop/modules/simplifycommerce/views/templates/hook/payment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21256671595720cf2a578899-50602113%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dc9e8660c238d2ec1381d420c06b391b439ebe26' => 
    array (
      0 => '/home/darkvictor/public_html/presta_shop/prestashop/modules/simplifycommerce/views/templates/hook/payment.tpl',
      1 => 1461767962,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21256671595720cf2a578899-50602113',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'simplify_public_key' => 0,
    'firstname' => 0,
    'lastname' => 0,
    'city' => 0,
    'address1' => 0,
    'address2' => 0,
    'state' => 0,
    'postcode' => 0,
    'module_dir' => 0,
    'show_saved_card_details' => 0,
    'customer_details' => 0,
    'payment_mode' => 0,
    'overlay_color' => 0,
    'hosted_payment_name' => 0,
    'hosted_payment_description' => 0,
    'hosted_payment_reference' => 0,
    'hosted_payment_amount' => 0,
    'show_save_customer_details_checkbox' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5720cf2a741e38_39132582',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5720cf2a741e38_39132582')) {function content_5720cf2a741e38_39132582($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/home/darkvictor/public_html/presta_shop/prestashop/tools/smarty/plugins/modifier.escape.php';
if (!is_callable('smarty_function_html_select_date')) include '/home/darkvictor/public_html/presta_shop/prestashop/tools/smarty/plugins/function.html_select_date.php';
?>
<script>
    var simplifyPublicKey = "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['simplify_public_key']->value, 'htmlall', 'UTF-8');?>
",
            simplifyFirstname = "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['firstname']->value, 'htmlall', 'UTF-8');?>
", simplifyLastname = "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['lastname']->value, 'htmlall', 'UTF-8');?>
", simplifyCity = "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['city']->value, 'htmlall', 'UTF-8');?>
", simplifyAddress1 = "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['address1']->value, 'htmlall', 'UTF-8');?>
", simplifyAddress2 = "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['address2']->value, 'htmlall', 'UTF-8');?>
", simplifyState = "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['state']->value, 'htmlall', 'UTF-8');?>
", simplifyPostcode = "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['postcode']->value, 'htmlall', 'UTF-8');?>
";
</script>
<div class="simplifyFormContainer box">
<div class="clearfix">
    <h3 class="left pay-by-credit-card">Pay by Credit Card</h3>
    <img alt="Secure Icon" class="secure-icon secure-icon-img" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
img/secure-icon.png"/>

    <div class="error-msg">
        <span id="simplify-test-mode-msg" class="test-msg">( TEST PAYMENT )</span>
        <span id="simplify-no-keys-msg" class="msg-container hidden">Payment Form not configured correctly. Please contact support.</span>
    </div>
</div>

<div id="simplify-ajax-loader">
    <span>Your payment is being processed...</span>
    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
img/ajax-loader.gif" alt="Loader Icon"/>
</div>

<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
payment.php" method="POST" id="simplify-payment-form">
<?php if (isset($_smarty_tpl->tpl_vars['show_saved_card_details']->value)){?>
    <div id="old-card-container" class='card-type-container selected clearfix'>
        <div class="first card-detail left">
            <div class='card-detail-label'>&nbsp;</div>
            <input class="left" type="radio" name='cc-type' value='old' checked='checked'/>
        </div>
        <div class="card-detail left">
            <div class='card-detail-label'>Card Type</div>
            <div class='card-detail-text'><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['customer_details']->value->card->type, 'htmlall', 'UTF-8');?>
</div>
        </div>
        <div class="card-detail left">
            <div class='card-detail-label'>Card Number</div>
            <div class='card-detail-text'>xxxx - xxxx - xxxx
                - <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['customer_details']->value->card->last4, 'htmlall', 'UTF-8');?>
</div>
        </div>
        <div class="card-detail left">
            <div class='card-detail-label'>Expiry Date</div>
            <div class='card-detail-text'>
                        <span class='left'><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['customer_details']->value->card->expMonth, 'htmlall', 'UTF-8');?>

                            / <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['customer_details']->value->card->expYear, 'htmlall', 'UTF-8');?>
</span>

                <div id="cc-deletion-container" class="right center">
                    <div>
                        <img id='trash-icon' src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
img/trash.png" alt="trash icon"
                             title="Delete Credit Card"/>
                    </div>
                    <div id="cc-confirm-deletion">
                        <div class='small pad-botom'>Delete Credit Card?</div>
                        <div>
                            <span id="confirm-cc-deletion">Yes</span>
                            <span id="cancel-cc-deletion">No</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div id="cc-deletion-msg">Your credit card has been deleted: <span id="cc-undo-deletion-lnk"
                                                                       class='underline'>Undo <img
                    alt="Secure Icon" class="secure-icon" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
img/undo.png"/></span></div>
<?php }?>
<div id="new-card-container" class='card-type-container clearfix'>
    <?php if (isset($_smarty_tpl->tpl_vars['show_saved_card_details']->value)){?>
        <div class="clearfix">
            <div class="first card-detail left">
                <input class="left" type="radio" name='cc-type' value='new'
                       <?php if (isset($_GET['simplify_error'])){?>checked='checked'<?php }?> />
            </div>
            <div class="card-detail left">
                <div class='card-detail-text'>New Credit Card</div>
            </div>
        </div>
    <?php }?>
    <!-- DO NOT REMOVE THIS INLINE STYLE. It needs to be there for showing 'New Card' section displayed -->
    <div id="simplify-cc-details" <?php if (isset($_smarty_tpl->tpl_vars['show_saved_card_details']->value)){?> style="display: <?php if (isset($_GET['simplify_error'])){?>block;<?php }else{ ?>none;<?php }?>"<?php }?> <?php if (isset($_smarty_tpl->tpl_vars['show_saved_card_details']->value)){?> class="indent"<?php }?>>
        <div class="simplify-payment-errors"><?php if (isset($_GET['simplify_error'])){?><?php echo smarty_modifier_escape($_GET['simplify_error'], 'html', 'UTF-8');?>
<?php }?></div>
        <a name="simplify_error" class="hidden"></a>
        <?php if ($_smarty_tpl->tpl_vars['payment_mode']->value=='hosted_payments'){?>
            <script type="text/javascript" src="https://www.simplify.com/commerce/simplify.pay.js"></script>
            <script type="text/javascript">
                //Hosted payments options
                var options = {
                    color: "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['overlay_color']->value, 'htmlall', 'UTF-8');?>
"
                };

                //if its non-HTTPS set the redirectUrl back to this page
                if (!document.location.href.match(/^https:\/\//)) {
                    //redirect back to payment step
                    if (!window.location.origin) { //IE don't have window.location.origin :(
                        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
                    }
                    options.redirectUrl = window.location.origin + window.location.pathname + '?controller=order?step=3';
                }

                $(function () {
                    var url = window.location.href;
                    var cardToken = urlParam('cardToken', url);
                    if (cardToken) {
                        toggleHostedPaymentButton(false);
                        var response = {
                            cardToken: cardToken
                        };
                        processHostedPaymentForm(response, url);
                    }
                    else {
                        initHostedPayments(options);

                        $('#simplify-hosted-payment-button').click(function () {
                            toggleHostedPaymentButton(false);

                            if (options.redirectUrl) {
                                if ($('#saveCustomer').is(':checked')) {
                                    options.redirectUrl += '&saveCustomer=true';
                                }
                                if ($("#cc-deletion-msg").is(':visible')) {
                                    options.redirectUrl += '&deleteCustomerCard=true';
                                }
                            }
                            initHostedPayments(options);
                        });
                    }
                });
            </script>
            <div>
                <button id="simplify-hosted-payment-button"
                        data-sc-key="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['simplify_public_key']->value, 'htmlall', 'UTF-8');?>
"
                        data-name="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['hosted_payment_name']->value, 'htmlall', 'UTF-8');?>
"
                        data-description="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['hosted_payment_description']->value, 'htmlall', 'UTF-8');?>
"
                        data-reference="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['hosted_payment_reference']->value, 'htmlall', 'UTF-8');?>
"
                        data-amount="<?php echo $_smarty_tpl->tpl_vars['hosted_payment_amount']->value;?>
"
                        data-operation="create.token"
                        data-customer-name="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['firstname']->value, 'htmlall', 'UTF-8');?>
 <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['lastname']->value, 'htmlall', 'UTF-8');?>
"
                        data-color="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['overlay_color']->value, 'htmlall', 'UTF-8');?>
">
                    Pay Now
                </button
            </div>
        <?php }else{ ?>
            <label>Card Number</label>
            <br/>
            <input type="text" size="20" autocomplete="off" class="simplify-card-number" autofocus/>
            <div>
                <div class="block-left">
                    <div class="clear"></div>
                    <label>Expiration (MM YYYY)</label>
                    <br/>

                    <div><?php echo smarty_function_html_select_date(array('display_days'=>false,'end_year'=>smarty_modifier_escape('+20', 'htmlall', 'UTF-8')),$_smarty_tpl);?>
</div>
                </div>
                <div>
                    <label>CVC</label><br/>
                    <input type="text" size="4" autocomplete="off" class="simplify-card-cvc" maxlength="4"/>
                    <a href="javascript:void(0)" class="simplify-card-cvc-info no-border">
                        What's this?
                        <div class="cvc-info">
                            The CVC (Card Validation Code) is a 3 or 4 digit code on the reverse side of Visa,
                            MasterCard and Discover cards and on the front of American Express cards.
                        </div>
                    </a>
                </div>
            </div>
        <?php }?>
        <br/>
        <?php if (isset($_smarty_tpl->tpl_vars['show_save_customer_details_checkbox']->value)){?>
            <div class="clearfix save-customer">
                <input type="checkbox" id="saveCustomer" name="saveCustomer">
                <span id="saveCustomerLabel">Save your credit card details securely?</span>
                <span id="updateCustomerLabel">Update your credit card details securely?</span>
            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['payment_mode']->value!='hosted_payments'){?>
            <div>
                <img alt="Secure Icon" class="payment-cards" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
img/credit-cards.png"/>
            </div>
        <?php }?>
    </div>
</div>
<?php if ($_smarty_tpl->tpl_vars['payment_mode']->value=='hosted_payments'){?>
    <input type="hidden" name="hostedPayments" value="true"/>
<?php }?>
<div>
    <button type="submit"
            class="right button btn btn-default standard-checkout button-medium simplify-submit-button">
        <span>Submit Payment <i class="icon-chevron-right"></i></span>
    </button>
</div>
</form>
</div>
</div>
<?php }} ?>