<?php /* Smarty version Smarty-3.1.13, created on 2016-05-16 17:34:49
         compiled from "/home/darkvictor/public_html/presta_shop/prestashop/modules/simplifycommerce/views/templates/hook/module-wrapper.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1547725594573a3cf9689903-05152363%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f51f4ea22582e8d27e41aab50546152d6bbcdde4' => 
    array (
      0 => '/home/darkvictor/public_html/presta_shop/prestashop/modules/simplifycommerce/views/templates/hook/module-wrapper.tpl',
      1 => 1461767962,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1547725594573a3cf9689903-05152363',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_dir' => 0,
    'requirements' => 0,
    'k' => 0,
    'requirement' => 0,
    'request_uri' => 0,
    'simplify_mode' => 0,
    'private_key_test' => 0,
    'public_key_test' => 0,
    'private_key_live' => 0,
    'public_key_live' => 0,
    'save_customer_details' => 0,
    'statuses_options' => 0,
    'status_options' => 0,
    'statuses' => 0,
    'status' => 0,
    'payment_mode' => 0,
    'overlay_color' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_573a3cf98cfb39_55624630',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573a3cf98cfb39_55624630')) {function content_573a3cf98cfb39_55624630($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/home/darkvictor/public_html/presta_shop/prestashop/tools/smarty/plugins/modifier.escape.php';
?>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
css/spectrum.css" rel="stylesheet" type="text/css" media="all"/>
<link href="//fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/spectrum.js"></script>

<div class="simplify-module-wrapper">
    <div class="simplify-module-header">
        <a href="https://www.simplify.com/" target="_blank" class="left">
            <img class="logo" src="//www.simplify.com/commerce/static/images/app-logo-pos.png"
                 alt="Simplify Commerce Logo" width="150" height="64"></a>

        <div class="header-title left">
            <h1><?php echo smartyTranslate(array('s'=>'Start accepting payments now.','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h1>

            <h2><?php echo smartyTranslate(array('s'=>'It’s that simple.','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h2>
        </div>
        <a href="https://www.simplify.com/commerce/partners/prestashop#/" target="_blank" class="btn right"><span><?php echo smartyTranslate(array('s'=>'Sign up for free','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</span></a>
    </div>
    <div class="section">
        <div class="clearfix">
            <div class="marketing left">
                <div class="w-container features item">
                    <img class="features item icon" src="//www.simplify.com/commerce/static/images/feature_signup.jpg"
                         alt="feature_signup.jpg">

                    <h1 class="features item h1"><?php echo smartyTranslate(array('s'=>'Easy sign up','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h1>

                    <p><?php echo smartyTranslate(array('s'=>'Click the "Sign up for free" button and become a Simplify merchant for free.','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="marketing left">
                <div class="w-container features item">
                    <img class="features item icon" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
img/feature_price.jpg"
                         alt="feature_signup.jpg">

                    <h1 class="features item h1"><?php echo smartyTranslate(array('s'=>'Simple pricing','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h1>

                    <p>No setup fees.<br>No monthly fees.<br>No minimum.</p>
                </div>
            </div>
            <div class="marketing left">
                <div class="w-container features item">
                    <img class="features item icon" src="//www.simplify.com/commerce/static/images/feature_funding.jpg"
                         alt="feature_signup.jpg">

                    <h1 class="features item h1"><?php echo smartyTranslate(array('s'=>'Two-day funding','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h1>

                    <p><?php echo smartyTranslate(array('s'=>'Deposits are made into your account in two business days for most transactions.','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</p>
                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="w-container admin-description-block">
                <b>Simplify Commerce</b>, built my MasterCard, a global leader in the payment industry, makes it easy for small businesses to accept online payments. From our hosted ‘Pay Now’ solution that allows merchants to share links socially to our mobile point of sale to recurring payment solutions, we include must-have features key to businesses.
                <ul>
                    <li><?php echo smartyTranslate(array('s'=>'Omni-channel payment solution for website, mobile and eCommerce store','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</li>
                    <li><?php echo smartyTranslate(array('s'=>'Accepting major card brands','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</li>
                    <li><?php echo smartyTranslate(array('s'=>'Quick two-day funding','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</li>
                    <li><?php echo smartyTranslate(array('s'=>'Highest Level 1 PCI certification','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</li>
                    <li><?php echo smartyTranslate(array('s'=>'Simple eInvoicing','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</li>
                    <li><?php echo smartyTranslate(array('s'=>'Recurring billing for monthly subscriptions','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="formContainer">
        <section class="technical-checks">
            <?php if ($_smarty_tpl->tpl_vars['requirements']->value['result']){?>
            <div class="conf">
                <h3><?php echo smartyTranslate(array('s'=>'Good news! Everything looks to be in order, start accepting credit card payments now.','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h3>
            </div>
            <?php }else{ ?>
                <h3><?php echo smartyTranslate(array('s'=>'Unfortunately, at least one issue is preventing you from using Simplify Commerce. Please fix the issue and reload this page.','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h3>

                <h2><?php echo smartyTranslate(array('s'=>'Technical Checks','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h2>

                <table cellspacing="0" cellpadding="0" class="simplify-technical">
                    <?php  $_smarty_tpl->tpl_vars['requirement'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['requirement']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['requirements']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['requirement']->key => $_smarty_tpl->tpl_vars['requirement']->value){
$_smarty_tpl->tpl_vars['requirement']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['requirement']->key;
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value!='result'){?>
                            <tr>
                                <td>
                                    <?php if ($_smarty_tpl->tpl_vars['requirement']->value['result']){?>
                                        <img src="../img/admin/ok.gif" alt=""/>
                                    <?php }else{ ?>
                                        <img src="../img/admin/forbbiden.gif" alt=""/>
                                    <?php }?>
                                </td>
                                <td>
                                    <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['requirement']->value['name'], 'htmlall', 'UTF-8');?>
<br/>
                                    <?php if (!$_smarty_tpl->tpl_vars['requirement']->value['result']&&isset($_smarty_tpl->tpl_vars['requirement']->value['resolution'])){?>
                                        <?php echo Tools::safeOutput(smarty_modifier_escape($_smarty_tpl->tpl_vars['requirement']->value['resolution'], 'htmlall', 'UTF-8'),true);?>

                                        <br/>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php }?>
                    <?php } ?>
                </table>
            <?php }?>
        </section>
        <br/>
        <?php if ((!'is_backward')){?>
        /* If 1.4 and no backward, then leave */
        <?php }else{ ?>
        <form action="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['request_uri']->value, 'UTF-8');?>
" method="post">
            <section class="simplify-settings">
                <h2>API Key Mode</h2>

                <div class="half container">
                    <div class="keyModeContainer">
                        <input class="radioInput" type="radio" name="simplify_mode" value="0"
                                <?php if (!$_smarty_tpl->tpl_vars['simplify_mode']->value){?>
                                    checked="checked"
                                <?php }?>
                                /><span>Test Mode</span>
                        <input class="radioInput" type="radio" name="simplify_mode" value="1"
                                <?php if ($_smarty_tpl->tpl_vars['simplify_mode']->value){?>
                                    checked="checked"
                                <?php }?>
                                /><span>Live Mode</span>
                    </div>
                    <p>

                    <div class="bold"><?php echo smartyTranslate(array('s'=>'Test Mode','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</div>
                    All transactions in test mode are test payments. You can test your installation using card numbers
                    from our
                    <a href="https://www.simplify.com/commerce/docs/tutorial/index#testing" target="_blank">list of test
                        card numbers</a>.
                    You cannot process real payments in test mode, so all other card numbers will be declined.</p>
                    <p>

                    <div class="bold"><?php echo smartyTranslate(array('s'=>'Live Mode','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</div>
                    All transactions made in live mode are real payments and will be processed accordingly.</p>
                </div>
                <h2><?php echo smartyTranslate(array('s'=>'Set Your API Keys','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h2>

                <div class="account-mode container">
                    <p>If you have not already done so, you can create an account by clicking the 'Sign up for free'
                        button in the top right corner.<br/>
                        Obtain both your private and public API Keys from: Account Settings -> API Keys and supply them
                        below.</p>
                </div>
                <div class="clearfix api-key-container">
                    <div class="clearfix api-key-title">
                        <div class="left"><h4 class="ng-binding"><?php echo smartyTranslate(array('s'=>'Test','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h4></div>
                    </div>
                    <div class="api-keys">
                        <div class="api-key-header clearfix">
                            <div class="left api-key-key"><?php echo smartyTranslate(array('s'=>'Private Key','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</div>
                            <div class="left api-key-key"><?php echo smartyTranslate(array('s'=>'Public Key','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</div>
                        </div>
                        <div class="api-key-box clearfix">
                            <div class="left api-key-key api-key ng-binding"><input type="password"
                                                                                    name="simplify_private_key_test"
                                                                                    value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['private_key_test']->value, 'htmlall', 'UTF-8');?>
"/>
                            </div>
                            <div class="left api-key-key api-key ng-binding"><input type="text"
                                                                                    name="simplify_public_key_test"
                                                                                    value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['public_key_test']->value, 'htmlall', 'UTF-8');?>
"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix api-key-container">
                    <div class="clearfix api-key-title">
                        <div class="left"><h4 class="ng-binding"><?php echo smartyTranslate(array('s'=>'Live','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h4></div>
                    </div>
                    <div class="api-keys">
                        <div class="api-key-header clearfix">
                            <div class="left api-key-key"><?php echo smartyTranslate(array('s'=>'Private Key','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</div>
                            <div class="left api-key-key"><?php echo smartyTranslate(array('s'=>'Public Key','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</div>
                        </div>
                        <div class="api-key-box clearfix">
                            <div class="left api-key-key api-key ng-binding"><input type="password"
                                                                                    name="simplify_private_key_live"
                                                                                    value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['private_key_live']->value, 'htmlall', 'UTF-8');?>
"/>
                            </div>
                            <div class="left api-key-key api-key ng-binding"><input type="text"
                                                                                    name="simplify_public_key_live"
                                                                                    value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['public_key_live']->value, 'htmlall', 'UTF-8');?>
"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="left half">
                        <h2><?php echo smartyTranslate(array('s'=>'Save Customer Details','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h2>

                        <div class="account-mode container">
                            <p>Enable customers to save their card details securely on Simplify's servers for future
                                transactions.</p>

                            <div class="saveCustomerDetailsContainer">
                                <input class="radioInput" type="radio" name="simplify_save_customer_details" value="1"
                                        <?php if ($_smarty_tpl->tpl_vars['save_customer_details']->value==1){?>
                                            checked="checked"
                                        <?php }?>
                                        /><span>Yes</span>
                                <input class="radioInput" type="radio" name="simplify_save_customer_details" value="0"
                                        <?php if ($_smarty_tpl->tpl_vars['save_customer_details']->value==0){?>
                                            checked="checked"
                                        <?php }?>
                                        /><span>No</span>
                            </div>
                        </div>
                    </div>
                    <div class="half container left">
                        <?php  $_smarty_tpl->tpl_vars['status_options'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['status_options']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['statuses_options']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['status_options']->key => $_smarty_tpl->tpl_vars['status_options']->value){
$_smarty_tpl->tpl_vars['status_options']->_loop = true;
?>
                            <h2><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['status_options']->value['label'], 'htmlall', 'UTF-8');?>
</h2>
                            <p>Choose the status for an order once the payment has been successfully processed by
                                Simplify.</p>
                            <div>
                                <select name="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['status_options']->value['name'], 'htmlall', 'UTF-8');?>
">
                                    <?php  $_smarty_tpl->tpl_vars['status'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['status']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['status']->key => $_smarty_tpl->tpl_vars['status']->value){
$_smarty_tpl->tpl_vars['status']->_loop = true;
?>
                                        <option value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['status']->value['id_order_state'], 'htmlall', 'UTF-8');?>
"
                                                <?php if ($_smarty_tpl->tpl_vars['status']->value['id_order_state']==$_smarty_tpl->tpl_vars['status_options']->value['current_value']){?>
                                                    selected="selected"
                                                <?php }?>
                                                ><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['status']->value['name'], 'htmlall', 'UTF-8');?>
</option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                        <div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="left">
                        <h2><?php echo smartyTranslate(array('s'=>'Payment Mode','mod'=>'simplifycommerce'),$_smarty_tpl);?>
</h2>

                        <div class="container">
                            <table>
                                <tr>
                                    <td>
                                        <select id="simplify_payment_mode" name="simplify_payment_mode">
                                            <option value="hosted_payments"
                                                    <?php if ($_smarty_tpl->tpl_vars['payment_mode']->value=='hosted_payment'){?>selected="selected"<?php }?>>
                                                Hosted Payments
                                            </option>
                                            <option value="standard"
                                                    <?php if ($_smarty_tpl->tpl_vars['payment_mode']->value=='standard'){?>selected="selected"<?php }?>>Standard
                                            </option>
                                        </select>
                                    </td>
                                    <td id="modal-overlay-config">
                                        <label for="modal-overlay-color" class="modal-overlay">Modal
                                            Overlay:</label><input name="simplify_overlay_color" type="text"
                                                                   id="modal-overlay-color" size="8"
                                                                   value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['overlay_color']->value, 'htmlall', 'UTF-8');?>
"/><input
                                                id="colorSelector" type="text"
                                                value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['overlay_color']->value, 'htmlall', 'UTF-8');?>
"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="hp-notes" colspan="2">
                                        To use hosted payments you must create a new API Key pair with the <b>'Enable hosted payments'</b> option selected.<br/>
                                        For more information, please visit this <a target="_new" href="https://www.simplify.com/commerce/docs/tools/hosted-payments">link</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clearfix"><input type="submit" class="settings-btn btn right" name="SubmitSimplify"
                                             value="Save Settings"/></div>
    </div>
    </section>
    </form>
    <?php }?>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var $modalOverlayColor = $('#modal-overlay-color');
        var $colorSelector = $("#colorSelector");
        var $modalOverlayConfig = $('#modal-overlay-config');
        var $simplifyPaymentMode = $('#simplify_payment_mode');

        function updateSimplifySettings() {
            enableOrDisableOverlaySetting();
        }

        function enableOrDisableOverlaySetting() {
            var disable = $simplifyPaymentMode.val() === 'standard';
            $modalOverlayConfig.css('opacity', disable ? 0.6 : 1.0);
            $colorSelector.spectrum(disable ? 'disable' : 'enable');
            if (disable) {
                $modalOverlayColor.attr('disabled', true);
            }
            else {
                $modalOverlayColor.removeAttr('disabled');
            }
        }

        $simplifyPaymentMode.change(enableOrDisableOverlaySetting);

        $('input:radio[name=simplify_mode]').click(updateSimplifySettings);

        function changeColor(color) {
            $modalOverlayColor.val(color.toHexString());
        }

        $colorSelector.spectrum({
            preferredFormat: "hex",
            showInput: true,
            move: changeColor,
            change: changeColor
        });

        $modalOverlayColor.change(function () {
            $colorSelector.spectrum('set', $(this).val());
        });

        updateSimplifySettings();
    });
</script>
<?php }} ?>