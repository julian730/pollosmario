<?php /* Smarty version Smarty-3.1.13, created on 2016-05-10 23:52:52
         compiled from "/home/darkvictor/public_html/presta_shop/prestashop/themes/theme915/order-opc-new-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1315756725571e6f15e341e0-27188776%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '515af32999d71c407c2f675a98ea12169c84f9ba' => 
    array (
      0 => '/home/darkvictor/public_html/presta_shop/prestashop/themes/theme915/order-opc-new-account.tpl',
      1 => 1462938748,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1315756725571e6f15e341e0-27188776',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_571e6f1631e171_70288904',
  'variables' => 
  array (
    'link' => 0,
    'back' => 0,
    'HOOK_CREATE_ACCOUNT_TOP' => 0,
    'guestInformations' => 0,
    'countries' => 0,
    'country' => 0,
    'state' => 0,
    'genders' => 0,
    'gender' => 0,
    'days' => 0,
    'day' => 0,
    'months' => 0,
    'k' => 0,
    'month' => 0,
    'years' => 0,
    'year' => 0,
    'newsletter' => 0,
    'dlv_all_fields' => 0,
    'field_name' => 0,
    'b2b_enable' => 0,
    'v' => 0,
    'sl_country' => 0,
    'postCodeExist' => 0,
    'stateExist' => 0,
    'dniExist' => 0,
    'one_phone_at_least' => 0,
    'inv_all_fields' => 0,
    'HOOK_CREATE_ACCOUNT_FORM' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_571e6f1631e171_70288904')) {function content_571e6f1631e171_70288904($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/home/darkvictor/public_html/presta_shop/prestashop/tools/smarty/plugins/modifier.escape.php';
?><div id="opc_new_account" class="opc-main-block">
	<div id="opc_new_account-overlay" class="opc-overlay" style="display: none;"></div> 
	<h1><span>1</span> <?php echo smartyTranslate(array('s'=>'Cuenta'),$_smarty_tpl);?>
</h1>
	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication',true,null,"back=order-opc"), ENT_QUOTES, 'UTF-8', true);?>
" method="post" id="login_form" class="std">
		<fieldset>
			<div class="titled_box"><h2><span><?php echo smartyTranslate(array('s'=>'Ya registrado?'),$_smarty_tpl);?>
</span></h2></div>
			<p><a href="#" class="button btn btn-default" id="openLoginFormBlock"><?php echo smartyTranslate(array('s'=>'Click aquí'),$_smarty_tpl);?>
</a></p>
			<div id="login_form_content" style="display:none;">
				<!-- Error return block -->
				<div id="opc_login_errors" class="error alert alert-danger" style="display:none;"></div>
				<!-- END Error return block -->
				<div class="form-group">
					<label for="login_email"><?php echo smartyTranslate(array('s'=>'Email'),$_smarty_tpl);?>
</label>
					<input type="text" id="login_email" class="form-control" name="email" />
				</div>
				<div class="form-group">
					<label for="login_passwd"><?php echo smartyTranslate(array('s'=>'Contraseña'),$_smarty_tpl);?>
</label>
					<input type="password" id="login_passwd" class="form-control" name="login_passwd" />
				</div>
                <div class="form-group">
                	<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('password',true), ENT_QUOTES, 'UTF-8', true);?>
" class="lost_password"><?php echo smartyTranslate(array('s'=>'Olvidaste la contraseña?'),$_smarty_tpl);?>
</a>
                </div>
				<div class="submit">
					<?php if (isset($_smarty_tpl->tpl_vars['back']->value)){?><input type="hidden" class="hidden" name="back" value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['back']->value, 'htmlall', 'UTF-8');?>
" /><?php }?>
					<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button btn btn-default" value="<?php echo smartyTranslate(array('s'=>'Login'),$_smarty_tpl);?>
" />
				</div>
			</div>
		</fieldset>
	</form>
	<form action="javascript:;" method="post" id="new_account_form" class="std" autocomplete="on" autofill="on">
		<fieldset>
        	<div id="opc_account_errors" class="error alert-danger" style="display:none;"></div>
			<div class="titled_box"><h2 id="new_account_title"><?php echo smartyTranslate(array('s'=>'Cliente nuevo'),$_smarty_tpl);?>
</h2></div>
			<div id="opc_account_choice">
				<div class="opc_float">
					<h4></h4>
					<p>
						<input type="button" class="exclusive_large btn btn-default" id="opc_guestCheckout" value="<?php echo smartyTranslate(array('s'=>'Guest checkout'),$_smarty_tpl);?>
" />
					</p>
				</div>

				<div class="opc_float">
					<h4><?php echo smartyTranslate(array('s'=>'Create your account today and enjoy:'),$_smarty_tpl);?>
</h4>
					<ul class="bullet">
						<li><?php echo smartyTranslate(array('s'=>'Personalized and secure access'),$_smarty_tpl);?>
</li>
						<li><?php echo smartyTranslate(array('s'=>'A fast and easy check out process'),$_smarty_tpl);?>
</li>
						<li><?php echo smartyTranslate(array('s'=>'Separate billing and shipping addresses'),$_smarty_tpl);?>
</li>
					</ul>
					<p>
						<input type="button" class="button_large btn btn-default" id="opc_createAccount" value="<?php echo smartyTranslate(array('s'=>'Create an account'),$_smarty_tpl);?>
" />
					</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="opc_account_form">
				<?php echo $_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_TOP']->value;?>

				<script type="text/javascript">
				// <![CDATA[
				idSelectedCountry = <?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['id_state']){?><?php echo intval($_smarty_tpl->tpl_vars['guestInformations']->value['id_state']);?>
<?php }else{ ?>false<?php }?>;
				<?php if (isset($_smarty_tpl->tpl_vars['countries']->value)){?>
					<?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['country']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value){
$_smarty_tpl->tpl_vars['country']->_loop = true;
?>
						<?php if (isset($_smarty_tpl->tpl_vars['country']->value['states'])&&$_smarty_tpl->tpl_vars['country']->value['contains_states']){?>
							countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
] = new Array();
							<?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['country']->value['states']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value){
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
								countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
].push({'id' : '<?php echo $_smarty_tpl->tpl_vars['state']->value['id_state'];?>
', 'name' : '<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['state']->value['name'], 'htmlall', 'UTF-8');?>
'});
							<?php } ?>
						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['country']->value['need_identification_number']){?>
							countriesNeedIDNumber.push(<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
);
						<?php }?>	
						<?php if (isset($_smarty_tpl->tpl_vars['country']->value['need_zip_code'])){?>
							countriesNeedZipCode[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
] = <?php echo $_smarty_tpl->tpl_vars['country']->value['need_zip_code'];?>
;
						<?php }?>
					<?php } ?>
				<?php }?>
				//]]>
					
					function vat_number()
					{
						if (($('#company').length) && ($('#company').val() != ''))
							$('#vat_number_block').show();
						else
							$('#vat_number_block').hide();
					}
					function vat_number_invoice()
					{
						if (($('#company_invoice').length) && ($('#company_invoice').val() != ''))
							$('#vat_number_block_invoice').show();
						else
							$('#vat_number_block_invoice').hide();
					}
					
					$(document).ready(function() {
						$('#company').on('input',function(){
							vat_number();
						});
						$('#company_invoice').on('input',function(){
							vat_number_invoice();
						});
						vat_number();
						vat_number_invoice();
						
						$('.id_state option[value=<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value['id_state'])){?><?php echo intval($_smarty_tpl->tpl_vars['guestInformations']->value['id_state']);?>
<?php }?>]').prop('selected', true);
						$('.id_state_invoice option[value=<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value['id_state_invoice'])){?><?php echo intval($_smarty_tpl->tpl_vars['guestInformations']->value['id_state_invoice']);?>
<?php }?>]').prop('selected', true);
						
					});
					
				</script>
				<!-- Error return block -->
				
				<!-- END Error return block -->
                <div class="row">
                	<div class="col-xs-12 col-sm-6">
				<!-- Account -->
				<input type="hidden" id="is_new_customer" name="is_new_customer" value="0" />
				<input type="hidden" id="opc_id_customer" name="opc_id_customer" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['id_customer']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['id_customer'];?>
<?php }else{ ?>0<?php }?>" />
				<input type="hidden" id="opc_id_address_delivery" name="opc_id_address_delivery" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['id_address_delivery']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['id_address_delivery'];?>
<?php }else{ ?>0<?php }?>" />
				<input type="hidden" id="opc_id_address_invoice" name="opc_id_address_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['id_address_delivery']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['id_address_delivery'];?>
<?php }else{ ?>0<?php }?>" />
				<p class="required form-group">
					<label for="email"><?php echo smartyTranslate(array('s'=>'Email'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="email" class="form-control" id="email" name="email" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['email']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['email'];?>
<?php }?>" />
				</p>
				<p class="required password is_customer_param form-group">
					<label for="passwd"><?php echo smartyTranslate(array('s'=>'Contraseña'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="password" class="form-control" name="passwd" id="passwd" />
					<small class="form_info"><?php echo smartyTranslate(array('s'=>'(5 caracteres min.)'),$_smarty_tpl);?>
</small>
				</p>
				<p class="radio required form-group bottom_indent clearfix" style="display:none;">
					<span class="radio_title"><?php echo smartyTranslate(array('s'=>'Title'),$_smarty_tpl);?>
</span>
					<?php  $_smarty_tpl->tpl_vars['gender'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gender']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['genders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gender']->key => $_smarty_tpl->tpl_vars['gender']->value){
$_smarty_tpl->tpl_vars['gender']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['gender']->key;
?>
						<input type="radio" name="id_gender" id="id_gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id_gender;?>
" value="<?php echo $_smarty_tpl->tpl_vars['gender']->value->id_gender;?>
" <?php if (isset($_POST['id_gender'])&&$_POST['id_gender']==$_smarty_tpl->tpl_vars['gender']->value->id_gender){?>checked="checked"<?php }?> />
						<label for="id_gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id_gender;?>
" class="top"><?php echo $_smarty_tpl->tpl_vars['gender']->value->name;?>
</label>
					<?php } ?>
				</p>
				<p class="required form-group">
					<label for="firstname"><?php echo smartyTranslate(array('s'=>'Nombre'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="text" class="form-control" id="customer_firstname" name="customer_firstname" onblur="$('#firstname').val($(this).val());" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['customer_firstname']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['customer_firstname'];?>
<?php }?>" />
				</p>
				<p class="required form-group">
					<label for="lastname"><?php echo smartyTranslate(array('s'=>'Apellido'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="text" class="form-control" id="customer_lastname" name="customer_lastname" onblur="$('#lastname').val($(this).val());" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['customer_lastname']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['customer_lastname'];?>
<?php }?>" />
				</p>
				<div class="form-group">
	<!-- 				<label><?php echo smartyTranslate(array('s'=>'Date of Birth'),$_smarty_tpl);?>
</label>     --> 
                    <div class="row">
                 <!--    <div class="col-xs-4">
                             <select id="days" name="days" class="form-control">
                                <option value="">-</option>
                                <?php  $_smarty_tpl->tpl_vars['day'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['day']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['days']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['day']->key => $_smarty_tpl->tpl_vars['day']->value){
$_smarty_tpl->tpl_vars['day']->_loop = true;
?>
                                    <option value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['day']->value, 'htmlall', 'UTF-8');?>
" <?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&($_smarty_tpl->tpl_vars['guestInformations']->value['sl_day']==$_smarty_tpl->tpl_vars['day']->value)){?> selected="selected"<?php }?>><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['day']->value, 'htmlall', 'UTF-8');?>
&nbsp;&nbsp;</option>
                                <?php } ?>
                            </select>
                            
                        </div> --> 
                        <div class="col-xs-4">
         <!--                    <select id="months" name="months" class="form-control">
                                <option value="">-</option>
                                <?php  $_smarty_tpl->tpl_vars['month'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['month']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['months']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['month']->key => $_smarty_tpl->tpl_vars['month']->value){
$_smarty_tpl->tpl_vars['month']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['month']->key;
?>
                                    <option value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['k']->value, 'htmlall', 'UTF-8');?>
" <?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&($_smarty_tpl->tpl_vars['guestInformations']->value['sl_month']==$_smarty_tpl->tpl_vars['k']->value)){?> selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>$_smarty_tpl->tpl_vars['month']->value),$_smarty_tpl);?>
&nbsp;</option>
                                <?php } ?>
                            </select>                       --> 
                        </div>                      
                        <div class="col-xs-4">
           <!--             <select id="years" name="years" class="form-control">
                                <option value="">-</option>
                                <?php  $_smarty_tpl->tpl_vars['year'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['year']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['years']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['year']->key => $_smarty_tpl->tpl_vars['year']->value){
$_smarty_tpl->tpl_vars['year']->_loop = true;
?>
                                    <option value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['year']->value, 'htmlall', 'UTF-8');?>
" <?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&($_smarty_tpl->tpl_vars['guestInformations']->value['sl_year']==$_smarty_tpl->tpl_vars['year']->value)){?> selected="selected"<?php }?>><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['year']->value, 'htmlall', 'UTF-8');?>
&nbsp;&nbsp;</option>
                                <?php } ?>
                            </select>                       --> 
                        </div>
                    </div>
				</div>
				<?php if (isset($_smarty_tpl->tpl_vars['newsletter']->value)&&$_smarty_tpl->tpl_vars['newsletter']->value){?>
		<!--		<p class="checkbox-inline">
					<input type="checkbox" name="newsletter" id="newsletter" value="1" <?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['newsletter']){?>checked="checked"<?php }?> autocomplete="off"/>
					<label for="newsletter"><?php echo smartyTranslate(array('s'=>'Sign up for our newsletter!'),$_smarty_tpl);?>
</label>
				</p>           -->
                <br />
				<p class="checkbox-inline ml_none" >
		<!--			<input type="checkbox"name="optin" id="optin" value="1" <?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['optin']){?>checked="checked"<?php }?> autocomplete="off"/>
					<label for="optin"><?php echo smartyTranslate(array('s'=>'Receive special offers from our partners!'),$_smarty_tpl);?>
</label>
				</p>                   -->
				<?php }?>
                </div>
                	<div class="col-xs-12 col-sm-6 top_up">
                <div class="titled_box">
                	<h2><span><?php echo smartyTranslate(array('s'=>'Registro de seguridad'),$_smarty_tpl);?>
</span></h2>
                </div>
				<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(false, null, 0);?>
				<?php $_smarty_tpl->tpl_vars['postCodeExist'] = new Smarty_variable(false, null, 0);?>
				<?php $_smarty_tpl->tpl_vars['dniExist'] = new Smarty_variable(false, null, 0);?>
				<?php  $_smarty_tpl->tpl_vars['field_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_name']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['dlv_all_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field_name']->key => $_smarty_tpl->tpl_vars['field_name']->value){
$_smarty_tpl->tpl_vars['field_name']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="company"&&$_smarty_tpl->tpl_vars['b2b_enable']->value){?>
				<p class="form-group">
					<label for="company"><?php echo smartyTranslate(array('s'=>'Company'),$_smarty_tpl);?>
</label>
					<input type="text" class="form-control" id="company" name="company" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['company']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['company'];?>
<?php }?>" />
				</p>
                <?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="vat_number"){?>	
				<div id="vat_number_block" style="display:none;">
					<p class="form-group">
						<label for="vat_number"><?php echo smartyTranslate(array('s'=>'VAT number'),$_smarty_tpl);?>
</label>
						<input type="text" class="form-control" name="vat_number" id="vat_number" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['vat_number']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['vat_number'];?>
<?php }?>" />
					</p>
				</div>
				<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="dni"){?>
				<?php $_smarty_tpl->tpl_vars['dniExist'] = new Smarty_variable(true, null, 0);?>
				<p class="form-group">
					<label for="dni"><?php echo smartyTranslate(array('s'=>'Identification number'),$_smarty_tpl);?>
</label>
					<input type="text" class="form-control" name="dni" id="dni" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['dni']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['dni'];?>
<?php }?>" />
					<span class="form_info"><?php echo smartyTranslate(array('s'=>'DNI / NIF / NIE'),$_smarty_tpl);?>
</span>
				</p>
				<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="firstname"){?>
				<p class="required form-group">
					<label for="firstname"><?php echo smartyTranslate(array('s'=>'Nombre'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="text" class="form-control" id="firstname" name="firstname" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['firstname']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['firstname'];?>
<?php }?>" />
				</p>
				<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="lastname"){?>
				<p class="required form-group">
					<label for="lastname"><?php echo smartyTranslate(array('s'=>'Apellido'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="text" class="form-control" id="lastname" name="lastname" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['lastname']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['lastname'];?>
<?php }?>" />
				</p>
				<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="address1"){?>
				<p class="required form-group">
					<label for="address1"><?php echo smartyTranslate(array('s'=>'Dirección'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="text" class="form-control" name="address1" id="address1" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['address1']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['address1'];?>
<?php }?>" />
				</p>
				<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="address2"){?>
				<p class="form-group is_customer_param">
					<label for="address2"><?php echo smartyTranslate(array('s'=>'Address (Line 2)'),$_smarty_tpl);?>
</label>
					<input type="text" class="form-control" name="address2" id="address2" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['address2']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['address2'];?>
<?php }?>" />
				</p>
				<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="postcode"){?>
                <?php $_smarty_tpl->tpl_vars['postCodeExist'] = new Smarty_variable(true, null, 0);?>
				<p class="required postcode form-group">
					<label for="postcode"><?php echo smartyTranslate(array('s'=>'Codigo postal'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="tel" class="form-control" name="postcode" id="postcode" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['postcode']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['postcode'];?>
<?php }?>" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
				</p>
				<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="city"){?>
				<p class="required form-group">
					<label for="city"><?php echo smartyTranslate(array('s'=>'Ciudad'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="text" class="form-control" name="city" id="city" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['city']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['city'];?>
<?php }?>" />
					
				</p>
				<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="country"||$_smarty_tpl->tpl_vars['field_name']->value=="Country:name"){?>
				<p class="required form-group">
					<label for="id_country"><?php echo smartyTranslate(array('s'=>'País'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<select name="id_country" id="id_country" class="form-control">
						<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_country'];?>
"<?php if ((isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['id_country']==$_smarty_tpl->tpl_vars['v']->value['id_country'])||(!isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['sl_country']->value==$_smarty_tpl->tpl_vars['v']->value['id_country'])){?> selected="selected"<?php }?>><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['v']->value['name'], 'htmlall', 'UTF-8');?>
</option>
						<?php } ?>
					</select>
				</p>
				<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="state"||$_smarty_tpl->tpl_vars['field_name']->value=='State:name'){?>
				<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(true, null, 0);?>
				<p class="required id_state form-group" style="display:none;"> 
					<label for="id_state"><?php echo smartyTranslate(array('s'=>'Estado'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<select name="id_state" id="id_state" class="form-control">
						<option value="">-</option>
					</select>
				</p>
				<?php }?>
				<?php } ?>
                <?php if (!$_smarty_tpl->tpl_vars['postCodeExist']->value){?>
				<p class="required postcode form-group unvisible">
					<label for="postcode"><?php echo smartyTranslate(array('s'=>'Zip / Postal code'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="text" class="form-control" name="postcode" id="postcode" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['postcode']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['postcode'];?>
<?php }?>" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
				</p>
				<?php }?>
				<?php if (!$_smarty_tpl->tpl_vars['stateExist']->value){?>
				<p class="required id_state form-group unvisible">
					<label for="id_state"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<select name="id_state" id="id_state" class="form-control">
						<option value="">-</option>
					</select>
				</p>
				<?php }?>
                <?php if (!$_smarty_tpl->tpl_vars['dniExist']->value){?>
				<p class="required form-group dni">
					<label for="dni"><?php echo smartyTranslate(array('s'=>'Identification number'),$_smarty_tpl);?>
 <sup>*</sup></label>
					<input type="text" class="form-control" name="dni" id="dni" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['dni']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['dni'];?>
<?php }?>" />
					<span class="form_info"><?php echo smartyTranslate(array('s'=>'DNI / NIF / NIE'),$_smarty_tpl);?>
</span>
				</p>
				<?php }?>
				<p class="textarea is_customer_param">
			<!--		<label for="other"><?php echo smartyTranslate(array('s'=>'Additional information'),$_smarty_tpl);?>
</label>                                                                              
					<textarea class="form-control" name="other" id="other" cols="26" rows="3"></textarea>               -->
				</p>
				<?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value){?>
			<!--		<p class="inline-infos required is_customer_param"><?php echo smartyTranslate(array('s'=>'You must register at least one phone number.'),$_smarty_tpl);?>
</p>  -->
				<?php }?>								
				<p class="form-group is_customer_param">
			<!--		<label for="phone"><?php echo smartyTranslate(array('s'=>'Home phone'),$_smarty_tpl);?>
</label>
					<input type="tel" class="form-control" name="phone" id="phone" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['phone']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['phone'];?>
<?php }?>" />  -->
				</p>
				<p class="<?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value){?>required <?php }?>form-group">
				<!--	<label for="phone_mobile"><?php echo smartyTranslate(array('s'=>'Mobile phone'),$_smarty_tpl);?>
<?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value){?> <sup>*</sup><?php }?></label>
					<input type="tel" class="form-control" name="phone_mobile" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['phone_mobile']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['phone_mobile'];?>
<?php }?>" />
				</p>                   -->
				<input type="hidden" name="alias" id="alias" value="<?php echo smartyTranslate(array('s'=>'My address'),$_smarty_tpl);?>
"/>

				<p class="checkbox-inline is_customer_param">
			<!--		<input type="checkbox" name="invoice_address" id="invoice_address"<?php if ((isset($_POST['invoice_address'])&&$_POST['invoice_address'])||(isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['invoice_address'])){?> checked="checked"<?php }?> autocomplete="off"/>
					<label for="invoice_address"><b><?php echo smartyTranslate(array('s'=>'Please use another address for invoice'),$_smarty_tpl);?>
</b></label>                                -->
				</p>

				<div id="opc_invoice_address" class="titled_box is_customer_param">
					<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(false, null, 0);?>
                    <?php $_smarty_tpl->tpl_vars['postCodeExist'] = new Smarty_variable(false, null, 0);?>
					<?php $_smarty_tpl->tpl_vars['dniExist'] = new Smarty_variable(false, null, 0);?>
					<h2><span><?php echo smartyTranslate(array('s'=>'Invoice address'),$_smarty_tpl);?>
</span></h2>
					<?php  $_smarty_tpl->tpl_vars['field_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_name']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['inv_all_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field_name']->key => $_smarty_tpl->tpl_vars['field_name']->value){
$_smarty_tpl->tpl_vars['field_name']->_loop = true;
?>
					<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="company"&&$_smarty_tpl->tpl_vars['b2b_enable']->value){?>
					<p class="form-group">
						<label for="company_invoice"><?php echo smartyTranslate(array('s'=>'Company'),$_smarty_tpl);?>
</label>
						<input type="text" class="form-control" id="company_invoice" name="company_invoice" value="" />
					</p>
					<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="vat_number"){?>
				  <div id="vat_number_block_invoice" class="is_customer_param" style="display:none;">
						<p class="form-group">
							<label for="vat_number_invoice"><?php echo smartyTranslate(array('s'=>'VAT number'),$_smarty_tpl);?>
</label>
							<input type="text" class="form-control" id="vat_number_invoice" name="vat_number_invoice" value="" />
						</p>
					</div>
                    <?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="dni"){?>
                    <?php $_smarty_tpl->tpl_vars['dniExist'] = new Smarty_variable(true, null, 0);?>
					<p class="form-group">
						<label for="dni_invoice"><?php echo smartyTranslate(array('s'=>'Identification number'),$_smarty_tpl);?>
</label>
						<input type="text" class="form-control" name="dni_invoice" id="dni_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['dni_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['dni_invoice'];?>
<?php }?>" />
						<small class="form_info"><?php echo smartyTranslate(array('s'=>'DNI / NIF / NIE'),$_smarty_tpl);?>
</small>
					</p>
					<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="firstname"){?>
					<p class="required form-group">
						<label for="firstname_invoice"><?php echo smartyTranslate(array('s'=>'First name'),$_smarty_tpl);?>
 <sup>*</sup></label>
						<input type="text" class="form-control" id="firstname_invoice" name="firstname_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['lastname_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['lastname_invoice'];?>
<?php }?>" />
					</p>
					<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="lastname"){?>
					<p class="required form-group">
						<label for="lastname_invoice"><?php echo smartyTranslate(array('s'=>'Last name'),$_smarty_tpl);?>
 <sup>*</sup></label>
						<input type="text" class="form-control" id="lastname_invoice" name="lastname_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['firstname_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['firstname_invoice'];?>
<?php }?>" />
					</p>
					<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="address1"){?>
					<p class="required form-group">
						<label for="address1_invoice"><?php echo smartyTranslate(array('s'=>'Address'),$_smarty_tpl);?>
 <sup>*</sup></label>
						<input type="text" class="form-control" name="address1_invoice" id="address1_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['address1_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['address1_invoice'];?>
<?php }?>" />
					</p>
					<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="address2"){?>
					<p class="form-group is_customer_param">
						<label for="address2_invoice"><?php echo smartyTranslate(array('s'=>'Address (Line 2)'),$_smarty_tpl);?>
</label>
						<input type="text" class="form-control" name="address2_invoice" id="address2_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['address2_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['address2_invoice'];?>
<?php }?>" />
					</p>
					<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="postcode"){?>
                    <?php $_smarty_tpl->tpl_vars['postCodeExist'] = new Smarty_variable(true, null, 0);?>
					<p class="required postcode form-group">
						<label for="postcode_invoice"><?php echo smartyTranslate(array('s'=>'Zip / Postal Code'),$_smarty_tpl);?>
 <sup>*</sup></label>
						<input type="text" class="form-control" name="postcode_invoice" id="postcode_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['postcode_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['postcode_invoice'];?>
<?php }?>" onkeyup="$('#postcode_invoice').val($('#postcode_invoice').val().toUpperCase());" />
					</p>
					<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="city"){?>
					<p class="required form-group">
						<label for="city_invoice"><?php echo smartyTranslate(array('s'=>'City'),$_smarty_tpl);?>
 <sup>*</sup></label>
						<input type="text" class="form-control" name="city_invoice" id="city_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['city_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['city_invoice'];?>
<?php }?>" />
					</p>
					<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="country"||$_smarty_tpl->tpl_vars['field_name']->value=="Country:name"){?>
					<p class="required form-group">
						<label for="id_country_invoice"><?php echo smartyTranslate(array('s'=>'Country'),$_smarty_tpl);?>
 <sup>*</sup></label>
						<select name="id_country_invoice" id="id_country_invoice" class="form-control">
							<option value="">-</option>
							<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_country'];?>
"<?php if ((isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['id_country_invoice']==$_smarty_tpl->tpl_vars['v']->value['id_country'])||(!isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['sl_country']->value==$_smarty_tpl->tpl_vars['v']->value['id_country'])){?> selected="selected"<?php }?>><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['v']->value['name'], 'htmlall', 'UTF-8');?>
</option>
							<?php } ?>
						</select>
					</p>
					<?php }elseif($_smarty_tpl->tpl_vars['field_name']->value=="state"||$_smarty_tpl->tpl_vars['field_name']->value=='State:name'){?>
					<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(true, null, 0);?>
				  <p class="required id_state_invoice form-group " style="display:none;">
						<label for="id_state_invoice"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
 <sup>*</sup></label>
						<select name="id_state_invoice" id="id_state_invoice" class="form-control">
							<option value="">-</option>
						</select>
					</p>
					<?php }?>
					<?php } ?>
                <?php if (!$_smarty_tpl->tpl_vars['postCodeExist']->value){?>
					<p class="required postcode_invoice form-group unvisible">
						<label for="postcode_invoice"><?php echo smartyTranslate(array('s'=>'Zip / Postal Code'),$_smarty_tpl);?>
 <sup>*</sup></label>
						<input type="text" class="form-control" name="postcode_invoice" id="postcode_invoice" value="" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
					</p>
					<?php }?>
				<?php if (!$_smarty_tpl->tpl_vars['stateExist']->value){?>
				  <p class="required id_state_invoice form-group unvisible">
						<label for="id_state_invoice"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
 <sup>*</sup></label>
						<select name="id_state_invoice" id="id_state_invoice" class="form-control">
							<option value="">-</option>
						</select>
					</p>
					<?php }?>
                <?php if (!$_smarty_tpl->tpl_vars['dniExist']->value){?>
                    <p class="required form-group dni_invoice">
                        <label for="dni_invoice"><?php echo smartyTranslate(array('s'=>'Identification number'),$_smarty_tpl);?>
 <sup>*</sup></label>
                        <input type="text" class="form-control" name="dni_invoice" id="dni_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['dni_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['dni_invoice'];?>
<?php }?>" />
                        <span class="form_info"><?php echo smartyTranslate(array('s'=>'DNI / NIF / NIE'),$_smarty_tpl);?>
</span>
                    </p>
                <?php }?>
                   <p class="textarea is_customer_param">
						<label for="other_invoice"><?php echo smartyTranslate(array('s'=>'Additional information'),$_smarty_tpl);?>
</label>
						<textarea class="form-control" name="other_invoice" id="other_invoice" cols="26" rows="3"></textarea>
				  </p>
					<?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value){?>
						<p class="inline-infos required is_customer_param"><?php echo smartyTranslate(array('s'=>'You must register at least one phone number.'),$_smarty_tpl);?>
</p>
					<?php }?>					
					<p class="form-group is_customer_param">
						<label for="phone_invoice"><?php echo smartyTranslate(array('s'=>'Home phone'),$_smarty_tpl);?>
</label>
						<input type="tel" class="form-control" name="phone_invoice" id="phone_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['phone_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['phone_invoice'];?>
<?php }?>" />
					</p>
					<p class="<?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value){?>required <?php }?> form-group">
						<label for="phone_mobile_invoice"><?php echo smartyTranslate(array('s'=>'Mobile phone'),$_smarty_tpl);?>
<?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value){?> <sup>*</sup><?php }?></label>
						<input type="tel" class="form-control" name="phone_mobile_invoice" id="phone_mobile_invoice" value="<?php if (isset($_smarty_tpl->tpl_vars['guestInformations']->value)&&$_smarty_tpl->tpl_vars['guestInformations']->value['phone_mobile_invoice']){?><?php echo $_smarty_tpl->tpl_vars['guestInformations']->value['phone_mobile_invoice'];?>
<?php }?>" />
					</p>
					<input type="hidden" name="alias_invoice" id="alias_invoice" value="<?php echo smartyTranslate(array('s'=>'My Invoice address'),$_smarty_tpl);?>
" />
				</div>
                </div>
                </div>
				<?php echo $_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_FORM']->value;?>

				<p class="submit">
					<input type="submit" class="exclusive button btn btn-default" name="submitAccount" id="submitAccount" value="<?php echo smartyTranslate(array('s'=>'Guardar'),$_smarty_tpl);?>
" />
				</p>
				<p id="opc_account_saved" style="display:none;">
					<?php echo smartyTranslate(array('s'=>'Account information saved successfully'),$_smarty_tpl);?>

				</p>
				<p class="required opc-required clearfix">
					<sup>*</sup><?php echo smartyTranslate(array('s'=>'Campo requerido'),$_smarty_tpl);?>

				</p>
				<!-- END Account -->
			</div>
		</fieldset>
	</form>
	<div class="clear"></div>
</div><?php }} ?>