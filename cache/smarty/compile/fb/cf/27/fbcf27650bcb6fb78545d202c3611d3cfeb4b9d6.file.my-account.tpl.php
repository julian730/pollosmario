<?php /* Smarty version Smarty-3.1.13, created on 2016-05-11 10:20:16
         compiled from "/home/darkvictor/public_html/presta_shop/prestashop/themes/theme915/my-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:268466481571ba2e1617721-38148133%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fbcf27650bcb6fb78545d202c3611d3cfeb4b9d6' => 
    array (
      0 => '/home/darkvictor/public_html/presta_shop/prestashop/themes/theme915/my-account.tpl',
      1 => 1462976411,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '268466481571ba2e1617721-38148133',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_571ba2e1a778c9_98019315',
  'variables' => 
  array (
    'account_created' => 0,
    'has_customer_an_address' => 0,
    'link' => 0,
    'returnAllowed' => 0,
    'voucherAllowed' => 0,
    'HOOK_CUSTOMER_ACCOUNT' => 0,
    'base_dir' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_571ba2e1a778c9_98019315')) {function content_571ba2e1a778c9_98019315($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Mi cuenta'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<h1><span><?php echo smartyTranslate(array('s'=>'Mi cuenta'),$_smarty_tpl);?>
</span></h1>
<?php if (isset($_smarty_tpl->tpl_vars['account_created']->value)){?>
	<p class="alert alert-info">
    	<button class="close" data-dismiss="alert" type="button">×</button>
		<?php echo smartyTranslate(array('s'=>'Tu cuenta se creo satisfactoriamente'),$_smarty_tpl);?>

	</p>
<?php }?>
<div class="titled_box">
<h2><span><?php echo smartyTranslate(array('s'=>'Bienvenido a tu cuenta, aquí puedes manejar toda tu información personal y tus ordenes'),$_smarty_tpl);?>
</span></h2>
<ul class="myaccount_lnk_list content_list">
	<?php if ($_smarty_tpl->tpl_vars['has_customer_an_address']->value){?>
    <!-- 	<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('address',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Add my first address'),$_smarty_tpl);?>
"><i class="icon-building"></i> <?php echo smartyTranslate(array('s'=>'Add my first address'),$_smarty_tpl);?>
</a>     </li>   -->
	<?php }?>
	<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('history',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Orders'),$_smarty_tpl);?>
"><i class="icon-list-ol"></i> <?php echo smartyTranslate(array('s'=>'Historia de mis ordenes y detalles'),$_smarty_tpl);?>
</a></li>
	<?php if ($_smarty_tpl->tpl_vars['returnAllowed']->value){?>
		<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-follow',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Merchandise returns'),$_smarty_tpl);?>
"><i class="icon-reply"></i> <?php echo smartyTranslate(array('s'=>'My merchandise returns'),$_smarty_tpl);?>
</a></li>
	<?php }?>
	<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-slip',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Credit slips'),$_smarty_tpl);?>
"><i class="icon-ban-circle"></i> <?php echo smartyTranslate(array('s'=>'Recibos de mi tarjeta de Creditos'),$_smarty_tpl);?>
</a></li>
	<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
"><i class="icon-building"></i> <?php echo smartyTranslate(array('s'=>'Mis Direcciones'),$_smarty_tpl);?>
</a></li>
	<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('identity',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
"><i class="icon-user"></i> <?php echo smartyTranslate(array('s'=>'Mi información personal'),$_smarty_tpl);?>
</a></li>
	<?php if ($_smarty_tpl->tpl_vars['voucherAllowed']->value){?>
		<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('discount',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Vouchers'),$_smarty_tpl);?>
"><i class="icon-tag"></i> <?php echo smartyTranslate(array('s'=>'My vouchers'),$_smarty_tpl);?>
</a></li>                    
	<?php }?>
	<?php echo $_smarty_tpl->tpl_vars['HOOK_CUSTOMER_ACCOUNT']->value;?>

</ul>
</div>
<ul class="footer_links clearfix">
<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
" title="<?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
"><i class="icon-home"></i><?php echo smartyTranslate(array('s'=>'Inicio'),$_smarty_tpl);?>
</a></li>
</ul><?php }} ?>