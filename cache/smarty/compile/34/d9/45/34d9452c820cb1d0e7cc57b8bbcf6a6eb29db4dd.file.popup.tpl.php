<?php /* Smarty version Smarty-3.1.13, created on 2016-04-21 15:38:49
         compiled from "/home/darkvictor/public_html/presta_shop/prestashop/modules/minicslider/views/templates/admin/popup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21584179057192c49c24f62-97334386%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '34d9452c820cb1d0e7cc57b8bbcf6a6eb29db4dd' => 
    array (
      0 => '/home/darkvictor/public_html/presta_shop/prestashop/modules/minicslider/views/templates/admin/popup.tpl',
      1 => 1461265967,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21584179057192c49c24f62-97334386',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'newsletter' => 0,
    'minic' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57192c49c4f6f2_45149968',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57192c49c4f6f2_45149968')) {function content_57192c49c4f6f2_45149968($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['newsletter']->value){?>
<div id="newsletter" class="popup">
	<div class="inner">
	    <div class="header">
	        <h4><?php echo smartyTranslate(array('s'=>'Help Us!','mod'=>'minicslider'),$_smarty_tpl);?>
</h4> <!-- Title -->
	        <span class="close-popup" data-popup="#newsletter">x</span> <!-- Close Button -->
	    </div>
	    <div class="popup-content"> <!-- Content -->
	        <div class="container"> <!-- Container -->
	            <p><?php echo smartyTranslate(array('s'=>'By clicking to the YES button you agree to share some basic information with us.','mod'=>'minicslider'),$_smarty_tpl);?>
</p>
	            <p><b><?php echo smartyTranslate(array('s'=>'Don`t worry we`ll be discrete with this information','mod'=>'minicslider'),$_smarty_tpl);?>
:</b></p>
	            <ul>
	                <li><?php echo smartyTranslate(array('s'=>'Domain','mod'=>'minicslider'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->tpl_vars['minic']->value['info']['domain'];?>
</b></li>
	                <li><?php echo smartyTranslate(array('s'=>'Version','mod'=>'minicslider'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->tpl_vars['minic']->value['info']['version'];?>
</b></li>
	                <li><?php echo smartyTranslate(array('s'=>'PS Version','mod'=>'minicslider'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->tpl_vars['minic']->value['info']['psVersion'];?>
</b></li>
	            </ul>
	            <form>
	                <p><?php echo smartyTranslate(array('s'=>'If you want to riecive news about our updates, new modules, give us your e-mail address.','mod'=>'minicslider'),$_smarty_tpl);?>
</p>
	                <div>
	                    <label><?php echo smartyTranslate(array('s'=>'Email','mod'=>'minicslider'),$_smarty_tpl);?>
:</label>
	                    <input type="text" id="sendInfoEmail" name="infoEmail" />
	                </div>  
	            </form>
	            <h3><?php echo smartyTranslate(array('s'=>'Thank you for your help!','mod'=>'minicslider'),$_smarty_tpl);?>
</h3>
	        </div> <!-- /END Container -->
	        <div class="buttons"> <!-- Buttons Holder -->
	            <a href="#" id="sendInfo" class="submit-popup button-small green" data-popup="#newsletter"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'minicslider'),$_smarty_tpl);?>
</a>  <!-- Action Button -->
	            <a href="#" id="cancelInfo" class="close-popup button-small grey" data-popup="#newsletter"><?php echo smartyTranslate(array('s'=>'No','mod'=>'minicslider'),$_smarty_tpl);?>
</a>  <!-- Close Button -->
	        </div> <!-- /END Buttons Holder -->
	    </div> <!-- /END Content -->
	</div>
</div>
<?php }?><?php }} ?>