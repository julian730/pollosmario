<?php /* Smarty version Smarty-3.1.13, created on 2016-04-21 23:18:11
         compiled from "/home/darkvictor/public_html/presta_shop/prestashop/themes/theme915/404.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1273128987571997f3c851f6-33695224%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8799206ad03218074090e7e9abc91be944b4aa24' => 
    array (
      0 => '/home/darkvictor/public_html/presta_shop/prestashop/themes/theme915/404.tpl',
      1 => 1461265954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1273128987571997f3c851f6-33695224',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'base_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_571997f4145274_65219534',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_571997f4145274_65219534')) {function content_571997f4145274_65219534($_smarty_tpl) {?>
<div class="pagenotfound titled_box">
	<h1><span><?php echo smartyTranslate(array('s'=>'This page is not available'),$_smarty_tpl);?>
</span></h1>

	<p class="alert alert-info">
    	<button class="close" data-dismiss="alert" type="button">×</button>
		<i class="icon-info-sign"></i><?php echo smartyTranslate(array('s'=>'We\'re sorry, but the Web address you\'ve entered is no longer available.'),$_smarty_tpl);?>

	</p>

	<h2><span><?php echo smartyTranslate(array('s'=>'To find a product, please type its name in the field below.'),$_smarty_tpl);?>
</span></h2>
	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="std">
		<fieldset>
			<p class="form-group">
				<label for="search"><?php echo smartyTranslate(array('s'=>'Search our product catalog:'),$_smarty_tpl);?>
</label>
				<input class="form-control" id="search_query" name="search_query" type="text" />
			</p>
            <input type="submit" name="Submit" value="OK" class="button_small btn btn-default" />
		</fieldset>
	</form>

	<p class="footer_link_bottom"><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
" title="<?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
"><i class="icon-home"></i> <?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
</a></p>
</div><?php }} ?>