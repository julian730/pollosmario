<?php /* Smarty version Smarty-3.1.13, created on 2016-05-18 18:23:46
         compiled from "/home/darkvictor/public_html/presta_shop/prestashop/themes/theme915/modules/blockpermanentlinks/blockpermanentlinks-header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1095649307571926726fb672-49432822%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '01bbfb082cf3863be612f31300419690181c0a87' => 
    array (
      0 => '/home/darkvictor/public_html/presta_shop/prestashop/themes/theme915/modules/blockpermanentlinks/blockpermanentlinks-header.tpl',
      1 => 1463610223,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1095649307571926726fb672-49432822',
  'function' => 
  array (
  ),
  'cache_lifetime' => 31536000,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5719267272aab3_99151473',
  'variables' => 
  array (
    'link' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5719267272aab3_99151473')) {function content_5719267272aab3_99151473($_smarty_tpl) {?>

<!-- Block permanent links module HEADER -->




<section class="header-box blockpermanentlinks-header">
    <ul id="header_links" class="hidden-xs">
          

<li id="header_link_sitemap"><a style="color:#FFFFFF;" class="header_links_sitemap" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('sitemap'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'sitemap','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'sitemap','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
</a></li>         
 
<li id="header_link_contact"><a class="header_links_contact" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('contact',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'contact','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'contact','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
</a></li>
        
        



 

        
    </ul>

    <div class="mobile-link-top header-button visible-xs">
        <h4 class="icon_wrapp">
             <span class="title-hed"></span>contact
        </h4>
        <ul id="mobilelink" class="list_header">
         
            <li id="header_link_contact"><a class="header_links_contact" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('contact',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'contact','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'contact','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
</a></li>
           
               
		</ul>
    </div>
</section>   

                                        
<!-- /Block permanent links module HEADER -->


<?php }} ?>