<?php /* Smarty version Smarty-3.1.13, created on 2016-04-21 15:38:49
         compiled from "/home/darkvictor/public_html/presta_shop/prestashop/modules/minicslider/views/templates/admin/javascript.tpl" */ ?>
<?php /*%%SmartyHeaderCode:155360894757192c4976af36-27099012%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1ba33629dc8057909a6c25ec6d9fe067ff5f36a3' => 
    array (
      0 => '/home/darkvictor/public_html/presta_shop/prestashop/modules/minicslider/views/templates/admin/javascript.tpl',
      1 => 1461265967,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '155360894757192c4976af36-27099012',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'minic' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57192c4978ca07_48109985',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57192c4978ca07_48109985')) {function content_57192c4978ca07_48109985($_smarty_tpl) {?><script type="text/javascript">
jQuery(window).load(function(){
    <?php if ($_smarty_tpl->tpl_vars['minic']->value['first_start']){?>
    // First start
    $('#newsletter').fadeIn();
    minic.newsletter(false);
    <?php }?>
});
jQuery(document).ready(function($) {
    // News Feed    
    $.getJSON('http://clients.minic.ro/process/feed?callback=?', function(feed){
        var version = '<?php echo $_smarty_tpl->tpl_vars['minic']->value['info']['version'];?>
';
        var name = '<?php echo $_smarty_tpl->tpl_vars['minic']->value['info']['module'];?>
';
        
        // Banner
        if(feed['modules'][name]['version'] != version){
            $('#banner').empty().html(feed['modules'][name]['update']);
        }else if(feed['modules'][name]['news']){
            $('#banner').empty().html(feed['modules'][name]['news']);
        }else{
            $('#banner').empty().html(feed['news']);
        }

        // Module list
        if(feed.modules){
            list = '';
            $.each(feed.modules, function() {
                
                list += '<li>';
                list += '<a href="'+ this.link +'" target="_blank" title="<?php echo smartyTranslate(array('s'=>'Click for more details','mod'=>'minicslider'),$_smarty_tpl);?>
">';
                list += '<img src="'+ this.logo +'">';
                list += '<p>';
                list += '<span class="title">'+ this.name +'</span>';
                list += '<span class="description">'+ this.description +'</span>';
                list += '<span class="price">'+ this.price +'</span>';
                list += '</p>';
                list += '</a>';
                list += '</li>';
            });
            
        }
        $('ul#module-list').html(list);
    });
});
</script><?php }} ?>