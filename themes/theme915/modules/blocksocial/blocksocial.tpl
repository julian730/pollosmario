{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<section class="block blocksocial col-sm-2">
	<h4>{l s='Follow us' mod='blocksocial'}<i class="icon-plus-sign"></i></h4>
	<ul class="list-footer toggle_content">
		<a class="iconbox facebook" data-original-title="facebook" data-placement="top" data-toggle="tooltip" href="https://www.facebook.com/pages/Pollos-Mario-Hempstead/138417322879368" title=""><i class="icon-facebook">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></a>

<a class="iconbox google-plus" data-original-title="google-plus" data-placement="top" data-toggle="tooltip" href="https://plus.google.com/b/105582964368835304156/105582964368835304156/about" title=""><i class="icon-google-plus">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></a>


	<a class="iconbox twitter" data-original-title="twitter" data-placement="top" data-toggle="tooltip" href="https://twitter.com/pollosmarioli" title=""><i class="icon-twitter">&nbsp;&nbsp;</i></a>


	{*	{if $twitter_url != ''}<li class="twitter"><a href="{$twitter_url|escape:html:'UTF-8'}">{l s='Twitter' mod='blocksocial'}</a></li>{/if}       *}
	{*	{if $rss_url != ''}<li class="rss"><a href="{$rss_url|escape:html:'UTF-8'}">{l s='RSS' mod='blocksocial'}</a></li>{/if}                          *}
	</ul>
</section>

