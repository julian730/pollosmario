<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cashondelivery}theme915>cashondelivery_1f9497d3e8bac9b50151416f04119cec'] = 'Efectivo cuando se entrege la orden';
$_MODULE['<{cashondelivery}theme915>validation_0c25b529b4d690c39b0831840d0ed01c'] = 'Finalizar orden';
$_MODULE['<{cashondelivery}theme915>validation_d538c5b86e9a71455ba27412f4e9ab51'] = 'Efectivo cuando se entrege la orden';
$_MODULE['<{cashondelivery}theme915>validation_8861c5d3fa54b330d1f60ba50fcc4aab'] = 'Tu escogiste el método de pago efectivo cuando se entrege la orden';
$_MODULE['<{cashondelivery}theme915>validation_e2867a925cba382f1436d1834bb52a1c'] = ' El monto total de su orden es';
$_MODULE['<{cashondelivery}theme915>validation_1f87346a16cf80c372065de3c54c86d9'] = 'iva incluido';
$_MODULE['<{cashondelivery}theme915>validation_0881a11f7af33bc1b43e437391129d66'] = ' Por favor confirmar su pedido haciendo clic en \"Confirmo mi pedido\"';
$_MODULE['<{cashondelivery}theme915>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Otros métodos de pago';
$_MODULE['<{cashondelivery}theme915>validation_46b9e3665f187c739c55983f757ccda0'] = 'Confirmo mi pedido';
$_MODULE['<{cashondelivery}theme915>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Tu orden';
$_MODULE['<{cashondelivery}theme915>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'esta completada.';
$_MODULE['<{cashondelivery}theme915>confirmation_8861c5d3fa54b330d1f60ba50fcc4aab'] = 'Usted elegido el método de pago efectivo cuando se entregue la orden.';
$_MODULE['<{cashondelivery}theme915>confirmation_e6dc7945b557a1cd949bea92dd58963e'] = 'Su pedido será enviado muy pronto.';
$_MODULE['<{cashondelivery}theme915>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'Para cualquier pregunta o para más información, póngase en contacto con nuestro';
$_MODULE['<{cashondelivery}theme915>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'Servicio al cliente.';
