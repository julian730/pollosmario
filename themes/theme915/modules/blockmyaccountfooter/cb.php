<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockmyaccountfooter}theme915>blockmyaccountfooter_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Mi cuenta';
$_MODULE['<{blockmyaccountfooter}theme915>blockmyaccountfooter_74ecd9234b2a42ca13e775193f391833'] = 'Mis pedidos';
$_MODULE['<{blockmyaccountfooter}theme915>blockmyaccountfooter_5973e925605a501b18e48280f04f0347'] = 'Mis retornos de mercancía';
$_MODULE['<{blockmyaccountfooter}theme915>blockmyaccountfooter_89080f0eedbd5491a93157930f1e45fc'] = 'Mi mercancía devuelta';
$_MODULE['<{blockmyaccountfooter}theme915>blockmyaccountfooter_9132bc7bac91dd4e1c453d4e96edf219'] = 'Mis vales descuento';
$_MODULE['<{blockmyaccountfooter}theme915>blockmyaccountfooter_e45be0a0d4a0b62b15694c1a631e6e62'] = 'Mis direcciones';
$_MODULE['<{blockmyaccountfooter}theme915>blockmyaccountfooter_b4b80a59559e84e8497f746aac634674'] = 'Administrar mi información personal';
$_MODULE['<{blockmyaccountfooter}theme915>blockmyaccountfooter_63b1ba91576576e6cf2da6fab7617e58'] = 'Mis datos personales';
$_MODULE['<{blockmyaccountfooter}theme915>blockmyaccountfooter_95d2137c196c7f84df5753ed78f18332'] = 'Mis vales';
