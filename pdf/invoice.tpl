<title>order</title>
{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div style="font-size: 8pt; color: #444">

<table>
	<tr><td>&nbsp;</td></tr>
</table>



<!-- ADDRESSES -->
<table style="width: 100%">
	<tr>
               <td style="width: 85%">
			{if !empty($delivery_address)}


				<table style="width: 100%">
					<tr>
						<td style="width: 50%">
							<span style="font-weight: bold; font-size: 10pt; color: #9E9F9E">{l s='Delivery Address' pdf='true'}</span><br />
							 {$delivery_address}
						</td>
						<td style="width: 50%">
							<span style="font-weight: bold; font-size: 10pt; color: #9E9F9E">{l s='Billing Address' pdf='true'}</span><br />
							 {$invoice_address}
						</td>
					</tr>
				</table>



			{else}
				<table style="width: 100%">
					<tr>

						<td style="width: 80%">
							<span style="font-weight: bold; font-size: 10pt; color: #9E9F9E">{l s='Address' pdf='true'}</span><br />
							 {$invoice_address}
						</td>
					</tr>
				</table>


			{/if}
		</td>
	</tr>
</table>













<!-- / ADDRESSES -->



<!-- PRODUCTS TAB -->

<table style="width: 100%">
	<tr>
		<td style="width: 15%; padding-right: 7px; text-align: right; vertical-align: top; font-size: 7pt;">
			<!-- CUSTOMER INFORMATION -->
			<br /><br /><b>{l s='Order Number:' pdf='true'}</b><br />
			{$order->getUniqReference()}<br />
			<br />
			<b>{l s='Order Date:' pdf='true'}</b><br />
			{dateFormat date=$order->date_add full=0}<br />
			<br />
			<b>{l s='Payment Method:' pdf='true'}</b><br />
			<table style="width: 100%;">
			{foreach from=$order_invoice->getOrderPaymentCollection() item=payment}
				<tr>
					<td style="width: 50%">{$payment->payment_method}</td>
					<td style="width: 50%">{displayPrice price=$payment->amount currency=$order->id_currency}</td>
				</tr>
			{foreachelse}
				<tr>
					<td>{l s='No payment' pdf='true'}</td>
				</tr>
			{/foreach}
			</table>



			<!-- / CUSTOMER INFORMATION -->
		</td>
		
	</tr>
</table>







<!-- / PRODUCTS TAB  -->

<table style="width: 100%">
	<tr>
	  <td style="width: 40%; text-align: right">
			<table style="width: 100%; font-size: 8pt;"><br />
				<tr style="line-height:4px;">
					<td width="37%" style="text-align: left; background-color: #4D4D4D; color: #FFF; padding-left: 15px; font-weight: bold; width: 30%">{l s='Product' pdf='true'}</td>
					<!-- unit price tax excluded is mandatory -->
					
					
					
					<td width="13%" style="background-color: #4D4D4D; color: #FFF; text-align: center; font-weight: bold; width: 15%">{l s='Qty' pdf='true'}</td>
					<td width="11%" style="background-color: #4D4D4D; color: #FFF; text-align: right; font-weight: bold; width: {if !$tax_excluded_display}15%{else}25%{/if}">
						{l s='$' pdf='true'}
						
					</td>
				</tr>
				<!-- PRODUCTS -->
				{foreach $order_details as $order_detail}
				{cycle values='#FFF,#DDD' assign=bgcolor}
				<tr style="line-height:6px;">
					<td style="text-align: left; width: 25%">{$order_detail.product_name}</td>
					<!-- unit price tax excluded is mandatory -->
					
					
					<td style="text-align: right; width: 10%">{if (isset($order_detail.reduction_amount) && $order_detail.reduction_amount > 0)}
						-{displayPrice currency=$order->id_currency price=$order_detail.reduction_amount}
					{else if (isset($order_detail.reduction_percent) && $order_detail.reduction_percent > 0)}
						-{$order_detail.reduction_percent}%
					{else}
					--
					{/if}
					</td>
					<td style="text-align: center; width: 10%">{$order_detail.product_quantity}</td>
					<td width="16%" style="width: 15%; text-align: right;  width: {if !$tax_excluded_display}15%{else}25%{/if}">
					{if $tax_excluded_display}
						{displayPrice currency=$order->id_currency price=$order_detail.total_price_tax_excl}
					{else}
						{displayPrice currency=$order->id_currency price=$order_detail.total_price_tax_incl}
					{/if}
					</td>
				</tr>
					{foreach $order_detail.customizedDatas as $customizationPerAddress}
						{foreach $customizationPerAddress as $customizationId => $customization}
							<tr style="line-height:6px;">
								<td style="line-height:3px; text-align: left; width: 31%; vertical-align: top">

										<blockquote>
											{if isset($customization.datas[$smarty.const._CUSTOMIZE_TEXTFIELD_]) && count($customization.datas[$smarty.const._CUSTOMIZE_TEXTFIELD_]) > 0}
												{foreach $customization.datas[$smarty.const._CUSTOMIZE_TEXTFIELD_] as $customization_infos}
													{$customization_infos.name}: {$customization_infos.value}
													{if !$smarty.foreach.custo_foreach.last}<br />
													{else}
													<div style="line-height:0.4pt">&nbsp;</div>
													{/if}
												{/foreach}
											{/if}

											{if isset($customization.datas[$smarty.const._CUSTOMIZE_FILE_]) && count($customization.datas[$smarty.const._CUSTOMIZE_FILE_]) > 0}
												{count($customization.datas[$smarty.const._CUSTOMIZE_FILE_])} {l s='image(s)' pdf='true'}
											{/if}
										</blockquote>
								</td>
							  <td style="text-align: right; width: 10%"></td>
								<td style="text-align: center; width: 10%; vertical-align: top">&nbsp;</td>
							  <td style="width: 10%; text-align: right;"></td>
							</tr>
						{/foreach}
					{/foreach}
				{/foreach}
				<!-- END PRODUCTS -->

				<!-- CART RULES -->
				{assign var="shipping_discount_tax_incl" value="0"}
				{foreach $cart_rules as $cart_rule}
					{if $cart_rule.free_shipping}
						{assign var="shipping_discount_tax_incl" value=$order_invoice->total_shipping_tax_incl}
					{/if}
					{cycle values='#FFF,#DDD' assign=bgcolor}
					<tr style="line-height:6px;background-color:{$bgcolor}" text-align="left">
						<td style="line-height:3px;text-align:left;width:60%;vertical-align:top" colspan="{if !$tax_excluded_display}5{else}4{/if}">{$cart_rule.name}</td>
						<td>
							{if $tax_excluded_display}
								- {$cart_rule.value_tax_excl}
							{else}
								- {$cart_rule.value}
							{/if}
						</td>
					</tr>
				{/foreach}
				<!-- END CART RULES -->
			</table>


<table style="width: 100%"><br />
	<tr>
		<td style="width: 30%">{l s='Product Total (Tax Excl.)' pdf='true'}</td>
		<td style="width: 25%">{displayPrice currency=$order->id_currency price=$order_invoice->total_products}</td>
	</tr>
    <br />
    <tr>
		<td style="width: 30%">{l s='Product Total (Tax Incl.)' pdf='true'}</td>
		<td style="width: 25%">{displayPrice currency=$order->id_currency price=$order_invoice->total_products_wt}</td>
	</tr>
    <br />
    <tr>
		<td style="width: 30%">{l s='Total' pdf='true'}</td>
		<td style="width: 25%">{displayPrice currency=$order->id_currency price=$order_invoice->total_paid_tax_incl}</td>
	</tr>
</table>






			<table style="width: 100%">
				{if (($order_invoice->total_paid_tax_incl - $order_invoice->total_paid_tax_excl) > 0)}
				<tr style="line-height:5px;">
					<td width="38%" style="width: 15%; text-align: right;">&nbsp;</td>
				</tr>

				<tr style="line-height:5px;">
					<td style="width: 15%; text-align: right;">&nbsp;</td>
				</tr>
				{else}
				<tr style="line-height:5px;">
					<td style="width: 15%; text-align: right;">&nbsp;</td>
				</tr>
				{/if}

				{if $order_invoice->total_discount_tax_incl > 0}
				<tr style="line-height:5px;">
					<td style="width: 15%; text-align: right;">&nbsp;</td>
				</tr>
				{/if}

				{if $order_invoice->total_wrapping_tax_incl > 0}
				<tr style="line-height:5px;">
					<td style="width: 15%; text-align: right;">&nbsp;</td>
				</tr>
				{/if}

				{if $order_invoice->total_shipping_tax_incl > 0}
				<tr style="line-height:5px;">
					<td style="width: 15%; text-align: right;">&nbsp;</td>
				</tr>
				{/if}

				{if ($order_invoice->total_paid_tax_incl - $order_invoice->total_paid_tax_excl) > 0}
				<tr style="line-height:5px;">
					<td style="width: 15%; text-align: right;">&nbsp;</td>
				</tr>
				{/if}

				<tr style="line-height:5px;">
					<td style="width: 15%; text-align: right;">&nbsp;</td>
				</tr>

			</table>

		</td>
	</tr>
</table>



{* 

<div style="line-height: 1pt">&nbsp;</div>

{$tax_tab}

{if isset($order_invoice->note) && $order_invoice->note}
<div style="line-height: 1pt">&nbsp;</div>


<table style="width: 100%">
	<tr>
		<td style="width: 15%"></td>
		<td style="width: 85%">{$order_invoice->note|nl2br}</td>
	</tr>
</table>

{/if}                         *}




{if isset($HOOK_DISPLAY_PDF)}
<div style="line-height: 1pt">&nbsp;</div>

<table style="width: 100%">
	<tr>
		<td style="width: 15%"></td>
		<td style="width: 85%">{$HOOK_DISPLAY_PDF}</td>
	</tr>
</table>
{/if}

</div>
