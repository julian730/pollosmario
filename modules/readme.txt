En los archivos de tu tienda Prestashop, 
entra en el directorio del theme que tienes activo: 
"../theme/tutheme"

All� encontrar�s 5 archivos que se llaman como �stos:
order-address.tpl
order-carrier.tpl
order-opc.tpl
order-opc-new-account.tpl
order-payment.tpl

Ren�mbralos o gu�rdalos en un directorio nuevo
por si necesitas recuperarlos m�s adelante.

Por �ltimo, sube estos 5 archivos y 
refresca la cach� de tu tienda y de tu navegador.

A�ade un producto a la cesta y comprueba 
tu nueva p�gina Easy Checkout ;)